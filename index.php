<?php
ini_set('display_error', 1);
include("config.php");
$name = $system["default"];
if(isset($_GET["portal"])&&$_GET["portal"]!=NULL)
{
	$name = $_GET["portal"];
}
$verzeichnis = openDir($portal[$name]["img"]);
$dateiList = array();
while ($file = readDir($verzeichnis)) {
 if ($file != "." && $file != "..") {
  $datei["name"]=$file;
  $datei["url"]=$portal[$name]["img"]."/".$file;
  $dateiList[] = $datei;
 }
}
closeDir($verzeichnis);

//Smarty
require 'libs/Smarty.class.php';

$smarty = new Smarty;

$smarty->assign('title',$portal[$name]["name"]);
$smarty->assign('pics', $dateiList);
$smarty->display('template/'.$portal[$name]["template"].'/overview.tpl');
?>
